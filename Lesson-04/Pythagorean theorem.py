import math

print("Вычесляем длину гипотинузы по теореме Пифагора")
cathetus1 = int(input("Введите длину первого катета: "))
cathetus2 = int(input("Введите длину второго катета: "))
hypotenuse = math.sqrt(cathetus1 ** 2 + cathetus2 ** 2)
result = round(hypotenuse, 2)
print("Длина нашей гипотинузы: " + str(result))